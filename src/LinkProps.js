import styled from '@emotion/styled'
import { Link } from 'react-router-dom'

const LinkProps = styled(Link)`
text-decoration: none;

&:focus, &:hover, &:visited, &:link, &:active {
    text-decoration: none;
`
export default LinkProps
