/** @jsx jsx */
/* eslint-disable react/prop-types */
import { css, jsx } from '@emotion/core'
import theme from './theme'

const Container = props => {
  return (
    <div
      className="grid"
      css={css`
        background-color: ${props.backgroundColor
          ? props.backgroundColor
          : '#FFFFFF'};
        padding: ${props.padding ? props.padding : '1.5% 7.5%'};
        margin: ${props.margin ? props.margin : '0px auto'};
        font-family: ${theme.fonts.primary};
      `}
    >
      {props.children}
    </div>
  )
}

const ContainerFluid = props => {
  return (
    <div
      className="grid"
      css={css`
        background-color: ${props.backgroundColor
          ? props.backgroundColor
          : '#FFFFFF'};
        padding: ${props.padding ? props.padding : '10px'};
        margin: ${props.margin ? props.margin : '0px'};
      `}
    >
      {props.children}
    </div>
  )
}

export { Container, ContainerFluid }
