/** @jsx jsx */
/* eslint-disable react/prop-types */
import { hot } from 'react-hot-loader/root'
import { jsx } from '@emotion/core'
import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './containers/header/Header'
import Footer from './containers/footer/Footer'
import Catlog from './Catlog'
import ProductDetails from './containers/catlog/ProductDetails'

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={Catlog} />
          <Route path="/items/:id" exact component={ProductDetails} />
        </Switch>
        <Footer />
      </BrowserRouter>
    )
  }
}

export default hot(App)
