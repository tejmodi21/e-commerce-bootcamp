import React from 'react'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'emotion-theming'
import App from './App'
import theme from './theme'
import { Provider } from 'react-redux'
import configureStore from './redux/store/configureStore'

const store = configureStore()

describe('App', () => {
  it('Renders without error', () => {
    render(
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <App />
        </Provider>
      </ThemeProvider>
    )
  })
})
