/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import PropTypes from 'prop-types'

const Image = props => {
  return (
    <img
      css={css`
        display: block;
        width: ${props.width ? props.width : '100%'};
        height: ${props.height ? props.height : 'auto'};
        margin: ${props.margin ? props.margin : '0px'};
        padding: ${props.padding ? props.padding : '0px'};
        border: ${props.border ? props.border : '1px solid'};
        border-radius: ${props.borderRadius ? props.borderRadius : '0px'};
      `}
      src={props.src}
      alt={props.alt}
      srcSet={props.srcSet}
    />
  )
}

Image.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  margin: PropTypes.string,
  padding: PropTypes.string,
  border: PropTypes.string,
  borderRadius: PropTypes.string,
  src: PropTypes.string,
  alt: PropTypes.string,
  srcSet: PropTypes.object
}

export default Image
