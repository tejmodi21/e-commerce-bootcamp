import axios from 'axios'

let api = 'http://localhost:4444/'

if (process.env.NODE_ENV === 'production') {
  api = 'https://hpwd-ecom-api.herokuapp.com/'
}

export default axios.create({
  baseURL: api
})
