import styled from '@emotion/styled'

const Button = styled.button`
  color: ${props => (props.color ? props.color : '#000000')};
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'white'};
  display: ${props => (props.display ? props.display : 'inline-block')};
  padding: ${props => (props.padding ? props.padding : '0.5rem 0')};
  margin: ${props => (props.margin ? props.margin : '0.5rem 1rem')};
  border: ${props => (props.border ? props.border : '1px solid')};
  height: ${props => (props.height ? props.height : 'auto')};
  width: ${props => (props.width ? props.width : 'auto')};
`

export default Button
