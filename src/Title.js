/** @jsx jsx */
/* eslint-disable react/prop-types */
import { css, jsx } from '@emotion/core'

const Title = props => {
  return (
    <div
      css={css`
        font-size: ${props.fontSize ? props.fontSize : '1rem'};
        color: ${props.color ? props.color : '#000000'};
        margin: ${props.margin ? props.margin : '0px'};
        padding: ${props.padding ? props.padding : '0px'};
        font-weight: ${props.fontWeight ? props.fontWeight : 'normal'};
      `}
    >
      {props.title}
    </div>
  )
}

export default Title
