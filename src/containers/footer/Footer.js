/** @jsx jsx */
import { Container } from '../../Container'
import { jsx, css } from '@emotion/core'
import Logo from '../../Logo'
import NavLinkProps from '../../NavLinkProps'
import Title from '../../Title'

const FooterLeft = () => {
  return (
    <div className="col-4">
      <div
        css={css`
          display: flex;
          flex-direction: column;
          padding: 10px;
        `}
      >
        <Logo padding="10px 20px" margin="0px 10px" />
        <p
          css={css`
            font-size: 0.9rem;
            padding: 10px 20px;
            margin: 0px 10px;
            color: #cecece;
          `}
        >
          High quality Scandinavian furniture made from eco-friendly materials.
          Designed for modern, minimalist apartments
        </p>
      </div>
    </div>
  )
}

const FooterColumnOne = () => {
  return (
    <div
      css={css`
        display: flex;
        flex-direction: column;
      `}
    >
      <div
        css={css`
          margin-bottom: 20px;
          padding: 10px;
        `}
      >
        <Title title="Shopping online" color="#123abb" fontWeight="bold" />
      </div>

      <ul>
        <NavLinkProps path="/" title="Order Status" />

        <NavLinkProps path="/" title="Shipping and Delivery" />

        <NavLinkProps path="/" title="Returns" />
        <NavLinkProps path="/" title="Payment Options" />
      </ul>
    </div>
  )
}

const Footer = () => {
  return (
    <div
      css={css`
        display: flex;
        color: lightgrey;
        padding: 10px 20px;
        font-size: 1.5rem;
        flex-direction: row;
        justify-content: flex-start;
      `}
    >
      <Container>
        <FooterLeft />
        <div className="col-8">
          <div
            css={css`
              display: flex;
              flex-direction: row;
              justify-content: space-evenly;
            `}
          >
            <FooterColumnOne />
            <FooterColumnOne />
            <FooterColumnOne />
          </div>
        </div>
      </Container>
    </div>
  )
}

export default Footer
