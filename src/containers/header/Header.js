/** @jsx jsx */
import Logo from '../../Logo'
import { jsx, css } from '@emotion/core'
import { ContainerFluid } from '../../Container'
import NavLinkProps from '../../NavLinkProps'

const Header = () => {
  return (
    <ContainerFluid
      backgroundColor="#000000"
      css={css`
        padding-top: 10px !important;
      `}
    >
      <div
        className="col-3"
        css={css`
          display: flex;
          align-items: flex-end;
        `}
      >
        <Logo />
      </div>
      <div className="col-4">
        <ul
          css={css`
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-end;
            margin-top: 10px;
            align-items: center;
          `}
        >
          <NavLinkProps
            path="/items"
            fontSize="1.5rem"
            color="#fefefe"
            title="Men"
          />
          <NavLinkProps
            path="/"
            fontSize="1.5rem"
            color="#fefefe"
            title="Women"
          />
          <NavLinkProps
            path="/"
            fontSize="1.5rem"
            color="#fefefe"
            title="Kids"
          />
        </ul>
      </div>

      <div className="col-5">
        <ul
          css={css`
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-end;
            margin-top: 10px;
          `}
        >
          <NavLinkProps
            path="/"
            fontSize="1.5rem"
            color="#fefefe"
            title="Search"
          />
          <NavLinkProps
            path="/"
            fontSize="1.5rem"
            color="#fefefe"
            title="Cart"
          />
          <NavLinkProps
            path="/"
            fontSize="1.5rem"
            color="#fefefe"
            title="Login"
          />
        </ul>
      </div>
    </ContainerFluid>
  )
}

export default Header
