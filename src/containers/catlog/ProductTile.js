/* eslint-disable react/prop-types */
/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import Image from '../../Image'
import Title from '../../Title'
import FavCTA from './FavCTA'
import Price from './Price'
import AddToCartButton from './AddToCartButton'
import LinkProps from '../../LinkProps'

const ImageWithFavIcon = props => {
  return (
    <div
      css={css`
        position: relative;
      `}
    >
      <div
        css={css`
          position: absolute;
          right: 0.5rem;
          margin: 1rem;
        `}
      >
        <FavCTA />
      </div>
      <LinkProps to={props.to}>
        <Image src={props.src} margin="0px 0px 3rem" />
      </LinkProps>
    </div>
  )
}

export const ProductTile = props => {
  return (
    <div className="col-3_md-4_xs-6">
      <ImageWithFavIcon src={props.src} to={props.to} />
      <div
        css={css`
          margin-top: 20px;
        `}
      >
        <LinkProps to={props.to}>
          <Title title={props.title} padding="5px 0px" fontSize="1.25rem" />

          <Price
            primary
            priceType={props.priceType}
            currentPrice={props.currentPrice}
            comparedPrice={props.comparedPrice}
          />
        </LinkProps>
        <AddToCartButton />
      </div>
    </div>
  )
}

// ProductTile.proTypes = {
//   src: PropTypes.string
// }

// Title.proTypes = {
//   title: PropTypes.string
// }

// ProductTile.Price.proTypes = {
//   salePrice: PropTypes.string,
//   price: PropTypes.string
// }

// Image.proTypes = {
//   src: PropTypes.string
// }

export default ProductTile
