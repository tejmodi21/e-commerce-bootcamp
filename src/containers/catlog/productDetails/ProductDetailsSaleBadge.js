/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const ProductDetailsSaleBadge = () => {
  return (
    <div
      css={css`
        display: inline-block;
        background-color: red;
        border-radius: 2rem;
        font-size: 1rem;
        margin: 0.5rem;
        padding: 1rem 1.5rem;
        font-weight: 700;
        color: white;
      `}
    >
      SALE
    </div>
  )
}

export default ProductDetailsSaleBadge
