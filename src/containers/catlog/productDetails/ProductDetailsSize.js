/** @jsx jsx */
import { jsx, css } from '@emotion/core'
//import PropTypes from 'prop-types'

const ProductDetailsSize = () => {
  return (
    <div
      css={css`
        font-size: 12px;
      `}
    >
      SIZE
    </div>
  )
}

export default ProductDetailsSize
