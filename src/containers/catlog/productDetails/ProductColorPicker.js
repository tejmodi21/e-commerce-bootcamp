/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import PropTypes from 'prop-types'

const Color = props => {
  return (
    <li
      css={css`
        display: block;
        width: 3rem;
        height: 3rem;
        border: 1px solid;
        margin: 0.5rem;

        border-color: #e6e6e6;
      `}
    >
      <button
        css={css`
          width: 3rem;
          height: 3rem;
        `}
      >
        <img src={props.src} alt={props.color} />
      </button>
    </li>
  )
}

const ProductColorPicker = props => {
  return (
    <div
      css={css`
        margin: 3rem 0rem;
      `}
    >
      <div>
        Color:
        <ul
          css={css`
            display: flex;
            flex-direction: row;
          `}
        >
          {props.color.map(
            (item, index) => {
              return (
                <Color key={index} color={item.color} src={item.swatch_url} />
              )
            }
            //
          )}
        </ul>
      </div>
    </div>
  )
}

ProductColorPicker.propTypes = {
  color: PropTypes.array
}

Color.propTypes = {
  backgroundColor: PropTypes.string,
  src: PropTypes.string,
  color: PropTypes.string
}

export default ProductColorPicker
