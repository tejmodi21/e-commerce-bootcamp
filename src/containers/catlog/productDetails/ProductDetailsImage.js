/** @jsx jsx */
import { jsx } from '@emotion/core'
import Image from '../../../Image'
import PropTypes from 'prop-types'
import { ContainerFluid } from '../../../Container'

const ProductImageGallery = props => {
  return (
    <div className="col-3">
      <Image src={props.src} />
    </div>
  )
}

const ProductImageCarousel = props => {
  return (
    <div className="col-9">
      <Image src={props.src} />
    </div>
  )
}

const ProductDetailsImage = props => {
  return (
    <ContainerFluid>
      <ProductImageGallery src={props.src} />
      <ProductImageCarousel src={props.src} />
    </ContainerFluid>
  )
}

ProductImageGallery.propTypes = {
  src: PropTypes.string
}

ProductImageCarousel.propTypes = {
  src: PropTypes.string
}

ProductDetailsImage.propTypes = {
  src: PropTypes.string
}

export default ProductDetailsImage
