/** @jsx jsx */
import React from 'react'
import { jsx, css } from '@emotion/core'
import Button from '../../../Button'

const QuantButton = css`
  border-radius: 25px;
  border: 1px solid;
  margin: 20px;
  padding: 0.5rem;
  display: inline-block;
`

const SignButton = css`
  border: 0px;
  outline: none;
  font-size: 2rem;
`

class ProductQuantitySelector extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      quantity: 5
    }
  }
  onIncClick = () => {
    this.setState({ quantity: this.state.quantity + 1 })
  }

  onDecClick = () => {
    this.setState({ quantity: this.state.quantity - 1 })
  }

  handleChange = e => {
    this.setState({ quantity: e.target.value })
  }

  render() {
    return (
      <div css={QuantButton}>
        <Button css={SignButton} onClick={this.onIncClick}>
          +
        </Button>
        <input
          type="number"
          value={this.state.quantity}
          onChange={this.handleChange}
          min="1"
          max="12"
          css={css`
            border: 0px;
            outline: none;
            text-align: center;
            padding: 0.5rem;
            font-size: 2rem;
            width: 2em;
            ::-webkit-outer-spin-button,
            ::-webkit-inner-spin-button {
              -webkit-appearance: none;
              margin: 0;
            }
          `}
        />

        <Button css={SignButton} onClick={this.onDecClick}>
          -
        </Button>
      </div>
    )
  }
}

export default ProductQuantitySelector
