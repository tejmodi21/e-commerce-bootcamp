/** @jsx jsx */
import { jsx } from '@emotion/core'
import { ContainerFluid } from '../../../Container'

const DescriptionDetail = () => {
  return <div className="col-6">Hey</div>
}

const DescriptionMaterialDetail = () => {
  return <div className="col-6">Material</div>
}

const Description = () => {
  return (
    <ContainerFluid>
      <DescriptionDetail />
      <DescriptionMaterialDetail />
    </ContainerFluid>
  )
}

export default Description
