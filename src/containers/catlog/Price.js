/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import PropTypes from 'prop-types'

const Price = props => {
  return (
    <div
      css={theme => css`
        font-family: ${props.primary ? theme.fonts.primary : 'sans-serif'};
        color: ${theme.colors.text};
        font-size: ${props.fontSize
          ? props.fontSize
          : `${theme.fontSizes[3]}px`};
        margin: ${props.margin};
      `}
    >
      {props.priceType === 'sale' ? (
        <div className="grid">
          <div className="col-3">
            <div
              css={theme => css`
                color: ${theme.colors.secondary};
              `}
            >
              {props.currentPrice}
            </div>
          </div>
          <div className="col-3">
            <div
              css={theme => css`
                color: ${theme.colors.primary};
              `}
            >
              {props.comparedPrice}
            </div>
          </div>
        </div>
      ) : (
        <div>{props.currentPrice}</div>
      )}
    </div>
  )
}

Price.propTypes = {
  priceType: PropTypes.string,
  theme: PropTypes.object,
  primary: PropTypes.bool,
  comparedPrice: PropTypes.string,
  currentPrice: PropTypes.string,
  fontSize: PropTypes.string,
  margin: PropTypes.string
}
export default Price
