/* eslint-disable jsx-a11y/label-has-associated-control */
/** @jsx jsx */
import React from 'react'
import { jsx, css } from '@emotion/core'
import Button from '../../Button'

class Pagination extends React.Component {
  render() {
    return (
      <div
        css={css`
          display: flex;
          flex-direction: row-wrap;
        `}
      >
        <div
          css={css`
            display: flex;
            justify-content: ;
          `}
        >
          <label
            css={css`
              margin: 10px;
            `}
          >
            Page
          </label>

          <input
            type="number"
            css={css`
              -webkit-appearance: none;
              -moz-appearance: textfield;
              padding: 0.5rem;
              width: 2.75rem;
            `}
            value={this.props.value}
            onChange={this.props.onChange}
          />
          <div
            css={css`
              margin: 10px;
              display: block;
            `}
          >
            z <span>5</span>
          </div>
        </div>
        <div
          css={css`
            display: flex;
            justify-items: center;
          `}
        >
          <Button
            border="0"
            css={css`
              text-decoration: underline;
            `}
            onClick={this.props.onNextClick}
          >
            Next
          </Button>
        </div>
      </div>
    )
  }
}

export default Pagination
