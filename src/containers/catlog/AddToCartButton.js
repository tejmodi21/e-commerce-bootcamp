/** @jsx jsx */
import { jsx, css } from '@emotion/core'
import Button from '../../Button'

const AddToCartButton = () => {
  return (
    <Button margin="0" border="0" height="17.5px">
      <div>
        <div
          css={css`
            display: inline-block;
            margin-right: 1.25rem;
          `}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="17.5"
            height="17.5"
            viewBox="0 0 17.5 17.5"
          >
            <g
              id="noun_cart_2102832_4_"
              data-name="noun_cart_2102832 (4)"
              transform="translate(-6 -6)"
            >
              <path
                id="Path_5"
                data-name="Path 5"
                d="M6.729,7.427H7.947l1.825,8.059a2.556,2.556,0,0,0-1.489,2.368A2.461,2.461,0,0,0,10.64,20.4h9.669a.714.714,0,1,0,0-1.427H10.64a1.15,1.15,0,0,1,0-2.244h9.669a.729.729,0,0,0,.686-.472l2.462-6.686a.7.7,0,0,0-.089-.652.734.734,0,0,0-.6-.3H9.711L9.244,6.559A.725.725,0,0,0,8.532,6h-1.8a.714.714,0,1,0,0,1.427Zm15,2.619L19.8,15.3H11.225l-1.191-5.259h11.7Z"
                transform="translate(0 0)"
              />
              <path
                id="Path_6"
                data-name="Path 6"
                d="M19.157,50a.813.813,0,1,0,0,1.625h1.031a.813.813,0,1,0,0-1.625Z"
                transform="translate(-7.925 -28.125)"
              />
              <path
                id="Path_7"
                data-name="Path 7"
                d="M37.609,50a.813.813,0,0,0,0,1.625h1.031a.813.813,0,0,0,0-1.625Z"
                transform="translate(-19.771 -28.125)"
              />
            </g>
          </svg>
        </div>
        <div
          css={theme => css`
            display: inline-block;
            font-size: 11px;
            padding: 5px;
            font-family: ${theme.fonts.primary};
            line-height: 17px;
            font-weight: ${theme.fontWeights.semiBold};
            text-transform: uppercase;
            &:hover {
              text-decoration: underline;
            }
          `}
        >
          Add To Cart
        </div>
      </div>
    </Button>
  )
}

export default AddToCartButton
