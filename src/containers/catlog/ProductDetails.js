/** @jsx jsx */
import { jsx } from '@emotion/core'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchItemDetails } from '../../redux/actions'

import { ContainerFluid } from '../../Container'
import ProductDetailsImage from './productDetails/ProductDetailsImage'
import ProductDetailsSaleBadge from './productDetails/ProductDetailsSaleBadge'
import Title from '../../Title'
import Price from './Price'
import ProductColorPicker from './productDetails/ProductColorPicker'
// import ProductQuantitySelector from './productDetails/ProductQuantitySelector'
// import Description from './productDetails/ProductDescription'

class ProductDetails extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      src: null
    }
  }

  componentDidMount() {
    this.props.fetchItemDetails(this.props.match.params.id)
  }

  saleBadge() {
    if (this.props.details.price.formatted_current_price_type === 'sale') {
      return (
        <>
          <ProductDetailsSaleBadge />
        </>
      )
    }
  }

  colorPicker() {
    if (this.props.details.swatches !== undefined) {
      return <ProductColorPicker color={this.props.details.swatches.color} />
    }
  }

  // productDetailsImage() {
  //   this.setState({ src: this.props.details.image })
  //   console.log(this.state.src)
  //   if (this.state.src !== null) {
  //     return <ProductDetailsImage src={this.state.src} />
  //   }
  // }

  render() {
    if (Object.keys(this.props.details).length === 0) {
      return <div>Loading.....</div>
    }

    //<ProductDetailsImage src={this.state.src} />

    return (
      <ContainerFluid>
        <div className="col-6">
          <ProductDetailsImage src={this.props.details.image} />
        </div>

        <div className="col-6">
          {this.saleBadge()}

          <Title
            title={this.props.details.title}
            fontSize="2.5rem"
            margin="0.5rem"
          />

          <Price
            primary
            priceType={this.props.details.price.formatted_current_price_type}
            currentPrice={this.props.details.price.formatted_current_price}
            comparedPrice={this.props.details.price.formatted_comparison_price}
            fontSize="2.5rem"
            margin="0.5rem"
          />

          {this.colorPicker()}
        </div>
      </ContainerFluid>
    )
  }
}

const mapStateToProps = state => {
  return { details: state.Products.details }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchItemDetails }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails)
