/** @jsx jsx */
/* eslint-disable react/prop-types */
import { jsx, css } from '@emotion/core'
import { NavLink } from 'react-router-dom'

const NavLinkProps = props => {
  return (
    <li>
      <NavLink
        css={css`
          color: ${props.color ? props.color : '#000000'};
          padding: ${props.padding ? props.padding : '10px'};
          font-size: ${props.fontSize ? props.fontSize : '1rem'};
          margin: ${props.margin ? props.margin : '0px'};
          text-decoration: none;
          &:focus,
          &:hover,
          &:active {
            text-decoration: underline;
          }
        `}
        to={props.path}
      >
        {props.title}
      </NavLink>
    </li>
  )
}

export default NavLinkProps
