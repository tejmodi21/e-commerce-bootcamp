import dataApi from '../../api/dataApi'
import {
  FETCH_ITEMS_SUCCESS,
  //FETCH_ITEMS_ERROR
  //   FETCH_ITEMS_DETAILS_SUCCESS,
  //   FETCH_ITEMS_DETAILS_ERROR
  NEXT_PAGE,
  FETCH_ITEM_DETAIL
} from '../constants'

export const fetchItemsDetails = id => async dispatch => {
  const response = await dataApi.get(`items/page/${id}/limit/24`)
  dispatch({
    type: FETCH_ITEMS_SUCCESS,
    data: response.data
  })
}

export const fetchNextPage = id => dispatch => {
  const response = fetchItemsDetails(id)
  dispatch({
    type: NEXT_PAGE,
    data: response
  })
}

export const fetchItemDetails = id => (dispatch, getState) => {
  const data = getState().Products.items.find(item => item.tcin === id)
  dispatch({
    type: FETCH_ITEM_DETAIL,
    data
  })
}
// const setItemsResults = data => ({
//   type: FETCH_ITEMS_SUCCESS,
//   data
// })

// const setItemsError = data => ({
//   type: FETCH_ITEMS_ERROR,
//   data
// })
