import {
  FETCH_ITEMS_SUCCESS,
  FETCH_ITEMS_ERROR,
  NEXT_PAGE,
  FETCH_ITEM_DETAIL
} from '../constants'

const initialState = {
  items: [],
  itemsError: null,
  details: {},
  detailsError: null,
  isLoading: false,
  item: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ITEMS_SUCCESS:
      //return { ...state, items: state.items.concat(action.data) }
      return { ...state, items: action.data }
    case FETCH_ITEMS_ERROR:
      return { ...state, detailsError: action.data }
    case NEXT_PAGE:
      return { ...state, items: action.data }
    case FETCH_ITEM_DETAIL:
      return {
        ...state,
        details: action.data
      }
    default:
      return state
  }
}
