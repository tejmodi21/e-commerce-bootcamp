import { combineReducers } from 'redux'
import itemsReducer from './itemsReducer'

const rootReducer = combineReducers({
  Products: itemsReducer
})

export default rootReducer
