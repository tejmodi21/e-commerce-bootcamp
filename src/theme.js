export default {
  colors: {
    text: '#000000',
    background: '#efefef',
    hoverHightlight: 'lightgreen',
    primary: '#CECECE',
    secondary: '#FF0000',
    muted: '#FBB03B'
  },
  fonts: {
    primary: 'Poppins, sans-serif',
    heading: 'Poppins, sans-serif',
    body: 'Poppins, sans-serif'
  },
  fontWeights: {
    light: 100,
    body: 400,
    heading: 700,
    semiBold: 600
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96]
}
