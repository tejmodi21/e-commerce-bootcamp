import React from 'react'
import ReactDOM from 'react-dom'
import 'core-js'
import { ThemeProvider } from 'emotion-theming'
import App from './App'
import { Provider } from 'react-redux'
import './styles.css'
import theme from './theme'
import configureStore from './redux/store/configureStore'

if (process.env.NODE_ENV === 'development') {
  const axe = require('react-axe')
  axe(React, ReactDOM, 1000)
}

const store = configureStore()

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('app')
)
