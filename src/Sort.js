/* eslint-disable jsx-a11y/label-has-associated-control */
/** @jsx jsx */
import { jsx, css } from '@emotion/core'

const Sort = () => {
  return (
    <form
      css={css`
        display: flex;
        padding: 1rem;
      `}
    >
      <label
        css={css`
          padding: 0.5rem;
          font-weight: bold;
        `}
      >
        Sort:
      </label>
      <select
        css={css`
          display: inline-block;
          width: 7rem;
          max-width: 100%;
          border: 1px solid;
          border-radius: 25px;
          color: #111;
          appearance: none;
          padding: 0.75rem;
          background: url('http://blog.idevelopweb.site/wp-content/uploads/2016/05/nw_selarw.png')
            no-repeat scroll 97.5% center;
        `}
      >
        <option value="Popular">POPULAR</option>
        <option value="Newest">NEWEST</option>
        <option value="Oldest">OLDEST</option>
      </select>
    </form>
  )
}

export default Sort
