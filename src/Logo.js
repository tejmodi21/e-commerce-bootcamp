/* eslint-disable react/prop-types */
import React from 'react'
import styled from '@emotion/styled'
import { NavLink } from 'react-router-dom'

const LogoText = styled(NavLink)`
  font-size: 1.5rem;
  padding: ${props => (props.padding ? props.padding : '10px')};
  margin: ${props => (props.margin ? props.margin : '10px')};
  display: ${props => (props.display ? props.display : 'block')};
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
  &:focus,
  &:hover,
  &:active {
    text-decoration: none;
  }
  &:focus,
  &:hover {
    color: #000000;
  }
`

const Logo = props => {
  return (
    <LogoText
      to="/"
      display={props.display}
      padding={props.padding}
      margin={props.margin}
    >
      I-Shop
    </LogoText>
  )
}

export default Logo
