/* eslint-disable react/prop-types */
import React from 'react'
import { connect } from 'react-redux'
import { Container } from './Container'

import { bindActionCreators } from 'redux'
import { fetchItemsDetails, fetchNextPage } from './redux/actions'
import ProductTile from './containers/catlog/ProductTile'
import Pagination from './containers/catlog/Pagination'

class Catlog extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 1,
      numberPerPage: 24,
      numberOfPages: 5
    }
  }

  componentDidMount() {
    // eslint-disable-next-line react/prop-types
    this.props.fetchItemsDetails(this.state.page)
  }

  renderList() {
    // eslint-disable-next-line react/prop-types
    if (this.props.items === undefined) {
      return <div>Loading.....</div>
    }
    return this.props.items.map((item, index) => {
      if (item.price.formatted_current_price_type === 'sale') {
        return (
          <ProductTile
            key={index}
            src={item.image}
            title={item.title}
            priceType={item.price.formatted_current_price_type}
            currentPrice={item.price.formatted_current_price}
            comparedPrice={item.price.formatted_comparison_price}
            id={item.tcin}
            to={`/items/${item.tcin}`}
          />
        )
      } else {
        return (
          <ProductTile
            key={index}
            src={item.image}
            title={item.title}
            priceType={item.price.formatted_current_price_type}
            currentPrice={item.price.formatted_current_price}
            id={item.tcin}
            to={`/items/${item.tcin}`}
          />
        )
      }
    })
  }

  onNextClick = () => {
    if (this.state.page < 5) {
      this.setState({ page: this.state.page + 1 })
      this.props.fetchItemsDetails(this.state.page)
    }
  }

  // onPrevClick = () => {
  //   if (this.state.page > 1) {
  //     this.setState({ page: this.state.page - 1 })
  //     this.props.fetchItemsDetails(this.state.page)
  //   }
  // }

  onChange = event => {
    this.setState({ page: event.target.value })
  }

  render() {
    return (
      <Container>
        {this.renderList()}
        <Pagination
          onNextClick={this.onNextClick}
          value={this.state.page}
          onChange={this.onChange}
        />
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return { items: state.Products.items }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchItemsDetails, fetchNextPage }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Catlog)
